FROM gitlab/gitlab-runner:alpine

#RUN apt-get update -y && \
#    apt-get upgrade -y && \
#    apt-get install jq -y && \
#    apt-get install -y ca-certificates curl apt-transport-https vim && \
#    apt-get clean && \
#    rm -rf /var/lib/apt/lists/*

# update ca root certs from internet
RUN apk add ca-certificates curl bash && \
    curl -LJO https://github.com/FlakM/gitlab-runner-cleaner/releases/download/0.0.2/gitlab-runner-cleaner.sh && \
    chmod +x gitlab-runner-cleaner.sh

COPY entrypoint.sh /
RUN chmod +x /entrypoint.sh && \
    rm -rf /entrypoint

ENTRYPOINT ["/usr/bin/dumb-init", "/entrypoint.sh"]
CMD ["run", "--user=gitlab-runner", "--working-directory=/home/gitlab-runner"]