#!/usr/bin/env bash

if [ -z "$CI_SERVER_URL" ]; then
  CI_SERVER_URL="https://gitlab.com/"
fi

if [ -z "$DOCKER_IMAGE" ]; then
  export DOCKER_IMAGE=docker:latest
fi

if [ -z "$REGISTRATION_TOKEN" ]; then
  echo "ERROR: Env REGISTRATION_TOKEN not set!"
  exit 1
fi

if [ "$ADMIN_TOKEN" != "" ]; then
  REGISTER_OTHER_PROJECTS="true"
  PROJECTS_STRING=`echo "$PROJECTS_TO_REGISTER" | tr ";" ","`
fi

MY_NAME="${CUSTOM_RUNNER_NAME:-$HOSTNAME}"
export RUNNER_NAME="$MY_NAME"

# gitlab-ci-multi-runner data directory
DATA_DIR="/etc/gitlab-runner"
CONFIG_FILE=${CONFIG_FILE:-$DATA_DIR/config.toml}
# custom certificate authority path
CA_CERTIFICATES_PATH=${CA_CERTIFICATES_PATH:-$DATA_DIR/certs/ca.crt}
LOCAL_CA_PATH="/usr/local/share/ca-certificates/ca.crt"

update_ca() {
  echo "Updating CA certificates..."
  cp "${CA_CERTIFICATES_PATH}" "${LOCAL_CA_PATH}"
  update-ca-certificates --fresh >/dev/null
}

if [ -f "${CA_CERTIFICATES_PATH}" ]; then
  # update the ca if the custom ca is different than the current
  cmp --silent "${CA_CERTIFICATES_PATH}" "${LOCAL_CA_PATH}" || update_ca
fi

echo "===> Trap SIGTERM to terminate Gitlab Runner..."
trap "gitlab-runner stop" SIGTERM

echo "===> Running gitlab-runner register, if no runner is registered..."
export REGISTER_NON_INTERACTIVE=true

# test if runner is already registered
if [[ ! $(cat $CONFIG_FILE | grep "url = \"$CI_SERVER_URL\"") ]]; then
  if [[ ! $(cat $CONFIG_FILE | grep "name = \"$RUNNER_NAME\"") ]]; then
    gitlab-runner register --executor=docker $REGISTER_EXTRA_ARGS
    if [ $? -gt 0 ]; then
      echo "===> ERROR: Registration failed!"
      exit 1
    else
      if [ "$REGISTER_OTHER_PROJECTS" == "true" ]; then
        export GITLAB_TOKEN="$ADMIN_TOKEN" 
        ./gitlab-runner-cleaner.sh register "$RUNNER_NAME" --byId=false --projects "$PROJECTS_STRING"
      fi
    fi
  else
    echo "==> Registration already happened. Skipping..."
  fi
else
  echo "==> Registration already happened. Skipping..."
fi

echo "===> Rewrite config file to disable caching..."
sed -i 's/disable_cache = false/disable_cache = true/g' /etc/gitlab-runner/config.toml
if [ $? -gt 0 ]; then
  echo "===> Cache disabling failed..."
  exit 1
fi

sed -i '/^.*volumes =.*$/d' /etc/gitlab-runner/config.toml
if [ $? -gt 0 ]; then
  echo "===> Cache Volume Removal failed..."
  exit 1
fi

echo "===> Current version of gitlab-runner:"
gitlab-runner --version

echo "===> Running gitlab-runner run..."
gitlab-runner run

# do this just if we really want this
if [[ "$UNREGISTER_RUNNER" == "true" ]]; then
  echo "===> Stopping and unregistering..."
  gitlab-runner unregister --name "$RUNNER_NAME"
fi
